package grafo;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class GrafoTest 
{

	@Test(expected = IllegalArgumentException.class)
	public void extremoInegativoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(-1, 3);	
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void extremoJnegativoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, -1);	
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void extremoIPasadoDeRangoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(5, 3);	
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void extremoJPasadoDeRangoTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, 5);	
	}	
	
	@Test(expected = IllegalArgumentException.class)
	public void extremosIgualesTest()
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(3, 3);	
	}	

	@Test
	public void agregarAristaTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 3);
		
		assertTrue(grafo.existeArista(1, 3));		
	}
	
	@Test
	public void aristaSinOrdenTest() 
	{
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(1, 3);
		
		assertTrue(grafo.existeArista(3, 1));		
	}
	
	@Test
	public void vecinosTest()
	{
		Grafo grafo = grafoPrueba();
		
		Set<Integer> losVecinos = grafo.vecinos(0);
		
		assertEquals (3 , losVecinos.size());
		assertTrue (losVecinos.contains(1));
		assertTrue (losVecinos.contains(2));
		assertTrue (losVecinos.contains(3));	
	}

	@Test
	public void unVecinoTest()
	{
		Grafo grafo = grafoPrueba();
		
		Set<Integer> losVecinos = grafo.vecinos(1);
		
		assertEquals (1 , losVecinos.size());
		assertTrue (losVecinos.contains(0));
	}
	
	private Grafo grafoPrueba() 
	{
		Grafo grafo = new Grafo(5);
	
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(3, 4);
		return grafo;
	}
}
