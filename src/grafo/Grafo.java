package grafo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo 
{
	// Representamos el grafo a trav�s de lista de vecinos
	private ArrayList<HashSet<Integer>> _vecinos;
	
	// El grafo se inicializa con todos su v�rtices aislados
	public Grafo(int vertice)
	{
		_vecinos = new ArrayList<HashSet<Integer>>(vertice);
		for (int i=0; i<vertice;++i)
			_vecinos.add(new HashSet<Integer>());
	}
	
	// Agrega la arista con extremos i,j
	public void agregarArista(int i, int j)
	{
		chequearExtremos(i,j);
		chequearExiste(i,j);
			
		_vecinos.get(i).add(j);
		_vecinos.get(j).add(i);
	}
	
	// Elimina una arista con extremos i,j
	public void eliminarArista(int i, int j)
	{
		chequearExtremos(i,j);
		chequearNoExiste(i,j);	
		
		_vecinos.get(i).remove(j);
		_vecinos.get(j).remove(i);		
	}
	
	// Informa si existe la arista (i,j)
	public boolean existeArista(int i, int j)
	{
		chequearExtremos(i,j);
		return _vecinos.get(i).contains(j);
	}
	
	// Vecinos de un v�rtice
	public Set<Integer> vecinos(int i)
	{
		chequearVertice(i);
		return _vecinos.get(i);
	}
	
	private void chequearExtremos(int i, int j) 
	{
		if (i < 0 || i >= tama�o()) 
			throw new IllegalArgumentException("Argumento err�neo, par�metro i " + i);
			
		if (j < 0 || j >= tama�o() )
			throw new IllegalArgumentException("Argumento err�neo, par�metro j " + j);
		
		if (i == j)
			throw new IllegalArgumentException("Argumento err�neo, par�metro i = j " + i);
	}
	
	private void chequearNoExiste(int i, int j) 
	{
		if ( _vecinos.get(i).contains(j) == false)
			throw new IllegalArgumentException("La arista (" + i + "," + j + ") no existe!");		
	}

	private void chequearExiste(int i, int j) 
	{
		if ( _vecinos.get(i).contains(j) == true)
			throw new IllegalArgumentException("La arista (" + i + "," + j + ") existe!");		
	}
	
	private void chequearVertice(int i) 
	{
		if (i < 0 || i >= tama�o()) 
			throw new IllegalArgumentException("El v�rtice i" + i + " no existe!");		
	}
	
	public int tama�o() 
	{
		return _vecinos.size();
	}
}
