package grafo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnalizadorDeConexion 
{
	private Grafo _grafo;
	
	public AnalizadorDeConexion(Grafo grafo)
	{
		_grafo = grafo;
	}
	
	public boolean esConexo()
	{
		return alcanzables(0).size() == _grafo.tama�o(); 
	}

	private Set<Integer> alcanzables(int origen) 
	{
		Set<Integer> marcados = new HashSet<Integer>();
		List<Integer> L = new ArrayList<Integer>();
		
		L.add(origen);
		while (L.size() > 0)
		{
			int vertice = L.get(0);
			marcados.add(vertice);
			
			for (Integer vecino: _grafo.vecinos(vertice))
			{
				if (!marcados.contains(vecino) && !L.contains(vecino))
					L.add(vecino);
			}
			L.remove(L.indexOf(vertice));
		}
		return marcados;
	}	
}