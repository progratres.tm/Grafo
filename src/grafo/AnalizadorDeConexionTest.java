package grafo;

import static org.junit.Assert.*;

import org.junit.Test;

public class AnalizadorDeConexionTest 
{
	@Test
	public void conexoTest() 
	{
		Grafo grafo = grafoConexo();
		
		AnalizadorDeConexion analizador = new AnalizadorDeConexion(grafo);
		assertTrue(analizador.esConexo());
	}
	
	@Test
	public void noConexoTest()
	{
		Grafo grafo = grafoNoConexo();
		
		AnalizadorDeConexion analizador = new AnalizadorDeConexion(grafo);
		assertFalse(analizador.esConexo());
	}
	
	private Grafo grafoConexo() 
	{
		Grafo grafo = new Grafo(5);
	
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(3, 4);
		return grafo;
	}

	private Grafo grafoNoConexo() 
	{
		Grafo grafo = new Grafo(5);
	
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		return grafo;
	}

}
